import "./App.css";
import { useCookies } from "react-cookie";
import { useState } from "react";

function App() {
  const [value, setValue] = useState("");
  const [time, setTime] = useState(0);
  const [show, setShow] = useState(false);
  const [cookies, setCookie, removeCookie] = useCookies(["user"]);

  // const d = new Date(Date.now() + setTime);

  // console.log(time, d);
  const addCookie = () => {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState === 4 && this.status === 200) {
        setCookie("Value", value, {
          path: "/",
          maxAge: time,
        });
      }
    };
    xhttp.open("GET", "ajax_info.txt", true);
    xhttp.send();
  };

  const removeCookies = () => {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState === 4 && this.status === 200) {
        removeCookie("Value");
      }
    };
    xhttp.open("GET", "ajax_info.txt", true);
    xhttp.send();
  };

  const showHide = () => {
    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
      if (this.readyState === 4 && this.status === 200) {
        return (document.getElementById(
          "div"
        ).innerHTML = `<p className='bg-white'>Cookie: ${cookies.Value}</p>`);
      }
    };
    xhttp.open("GET", "ajax_info.txt", true);
    xhttp.send();
  };

  return (
    <div className="App flex flex-col w-full justify-center items-center bg-slate-500 h-screen">
      <div className="flex flex-col gap-3">
        <div>
          <h2 className="text-lg">set Value:</h2>
          <input
            type="text"
            value={value}
            className="border px-3"
            onChange={(e) => setValue(e.target.value)}
          />
        </div>
        <div id="div"></div>
        <div>
          <h2 className="text-lg">Time</h2>
          <input
            type="text"
            id="de"
            className="border px-3"
            onChange={(e) => setTime(e.target.value)}
            value={time}
          />
        </div>
      </div>
      <div className="flex gap-3 mt-10">
        <button
          className="border px-3 py-2 rounded-lg text-white bg-purple-500 hover:bg-white hover:text-black"
          onClick={addCookie}
        >
          setCookie
        </button>
        <button
          className="border px-3 py-2 rounded-lg text-white bg-purple-500 hover:bg-white hover:text-black"
          // onClick={() => setShow(!show)}
          onClick={showHide}
        >
          getCookie
        </button>
        <button
          className="border px-3 py-2 rounded-lg text-white bg-purple-500 hover:bg-white hover:text-black"
          onClick={removeCookies}
        >
          clearCookie
        </button>
      </div>
    </div>
  );
}

export default App;
